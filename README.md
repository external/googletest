This mirror has been deprecated. The actual repository has moved to
https://github.com/google/googletest/
The new mirror of it is
https://chromium.googlesource.com/external/github.com/google/googletest/
